myBuild:
    stage: build
    script:
        - echo "Build"
    
myDeployTrigger:dev:
    stage: deploy
    image: psoders/docker-alpine-curl
    script:   
        - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.com/api/v4/projects/<myDeployPipeline_project_id>/trigger/pipeline
    only:
        - master
